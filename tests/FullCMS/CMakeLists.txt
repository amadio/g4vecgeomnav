include(${Geant4_USE_FILE})

file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cc)

add_executable(full_cms full_cms.cc ${sources})

target_include_directories(
  full_cms
        PUBLIC
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
)

target_link_libraries(
        full_cms g4vecgeomnav
)

#----------------------------------------------------------------------------
# Copy all scripts to the build/OUTPUT directory. This is so that, after
# install, we can run the executable directly because it relies on these
# scripts being in the current working directory.
#
# set(FULLCMS_SCRIPTS
#  bench.g4
#  cms.gdml
#  README
#  )

#foreach(_script ${FULLCMS_SCRIPTS})
#  configure_file(
#    ${_script}
#    ${CMAKE_BINARY_DIR}/${OUTPUT}/${_script}
#    COPYONLY
#    )
#endforeach()

#----------------------------------------------------------------------------
# Install the executable to 'bin/examples/FullCMS/Geant4' directory under the
# CMAKE_INSTALL_PREFIX
#
set(OUTPUT bin/examples/FullCMS/)
install(TARGETS full_cms DESTINATION ${OUTPUT})
# install(FILES ${FULLCMS_SCRIPTS} DESTINATION ${OUTPUT})
