// @(#)root/g4root:$Id$
// Author: Andrei Gheata   07/08/06

/// \file TG4VecGeomNavMgr.cxx
/// \brief Implementation of the TG4VecGeomNavMgr class
///
/// \author S. Wenzel; CERN

#include "TG4VecGeomNavigator.h"
#include "TG4VecGeomDetectorConstruction.h"
#include "TG4VecGeomNavMgr.h"

#include "TString.h"
#include "TError.h"

#include "G4RunManager.hh"
#include "G4TransportationManager.hh"
#include "G4PropagatorInField.hh"

G4ThreadLocal TG4VecGeomNavMgr *TG4VecGeomNavMgr::fVecGeomNavMgr = nullptr;
TG4VecGeomNavMgr *TG4VecGeomNavMgr::fgMasterInstance             = nullptr;

//______________________________________________________________________________
TG4VecGeomNavMgr::TG4VecGeomNavMgr(GeoManager_t const &geom, TG4VecGeomDetectorConstruction *detConstruction)
    : fVecGeomGeoMgr(&geom), fNavigator(nullptr), fDetConstruction(detConstruction), fConnected(false)
{
  /// Default ctor.
  if (!detConstruction) {
    // fDetConstruction = new TG4VecGeomDetectorConstruction(geom);
  }
  SetNavigator(new TG4VecGeomNavigator(detConstruction->GetFastG4VecGeomLookup()));
}

//______________________________________________________________________________
TG4VecGeomNavMgr::~TG4VecGeomNavMgr()
{
  /// Destructor.
  if (fNavigator) delete fNavigator;
  if (fDetConstruction) delete fDetConstruction;
  fVecGeomNavMgr = nullptr;

  G4bool isMaster = !G4Threading::IsWorkerThread();
  if (isMaster) {
    fgMasterInstance = 0;
  }
}

//______________________________________________________________________________
TG4VecGeomNavMgr *TG4VecGeomNavMgr::GetInstance(GeoManager_t const &geom)
{
  /// Get the pointer to the singleton. If none, create one based on 'geom'.
  if (fVecGeomNavMgr) return fVecGeomNavMgr;
  fVecGeomNavMgr  = new TG4VecGeomNavMgr(geom);
  G4bool isMaster = !G4Threading::IsWorkerThread();
  Printf("isMaster=%d", isMaster);
  if (isMaster) {
    fgMasterInstance = fVecGeomNavMgr;
  }
  return fVecGeomNavMgr;
}

//______________________________________________________________________________
TG4VecGeomNavMgr *TG4VecGeomNavMgr::GetInstance(const TG4VecGeomNavMgr &navMgr)
{
  /// Get the pointer to the singleton. If none, create one based on 'geom'.
  if (fVecGeomNavMgr) return fVecGeomNavMgr;
  // Check if we have to create one.
  fVecGeomNavMgr  = new TG4VecGeomNavMgr(*navMgr.fVecGeomGeoMgr, navMgr.fDetConstruction);
  G4bool isMaster = !G4Threading::IsWorkerThread();
  if (isMaster) {
    fgMasterInstance = fVecGeomNavMgr;
  }
  return fVecGeomNavMgr;
}

//_____________________________________________________________________________
TG4VecGeomNavMgr *TG4VecGeomNavMgr::GetMasterInstance()
{
  /// Get master instance
  return fgMasterInstance;
}

//______________________________________________________________________________
bool TG4VecGeomNavMgr::ConnectToG4()
{
  /// Connect detector construction class to G4 run manager.
  if (fConnected) {
    Info("ConnectToG4", "Already connected");
    return true;
  }
  if (!fDetConstruction) {
    Error("ConnectToG4", "No detector construction set !");
    return false;
  }
  if (!fNavigator) {
    Error("ConnectToG4", "Navigator has to be created before connecting to G4 !!!");
    return false;
  }
  auto runManager = G4RunManager::GetRunManager();
  if (!runManager) {
    Error("ConnectToG4", "Unable to connect: G4RunManager not instantiated");
    return false;
  }
  G4bool isMaster = !G4Threading::IsWorkerThread();
  if (isMaster) runManager->SetUserInitialization(fDetConstruction);
  Info("ConnectToG4", "VecGeom detector construction class connected to G4RunManager");
  fConnected = true;
  return true;
}

//______________________________________________________________________________
void TG4VecGeomNavMgr::SetNavigator(TG4VecGeomNavigator *nav)
{
  /// Connect a navigator to G4.
  if (fConnected) {
    Error("SetNavigator", "Navigator set after instantiation of G4RunManager. Won't set!!!");
    return;
  }
  G4TransportationManager *trMgr = G4TransportationManager::GetTransportationManager();
  //   G4Navigator *oldNav = trMgr->GetNavigatorForTracking();
  trMgr->SetNavigatorForTracking(nav);
  G4FieldManager *fieldMgr = trMgr->GetPropagatorInField()->GetCurrentFieldManager();
  delete trMgr->GetPropagatorInField();
  trMgr->SetPropagatorInField(new G4PropagatorInField(nav, fieldMgr));
  trMgr->ActivateNavigator(nav);
  G4EventManager *evtMgr = G4EventManager::GetEventManager();
  if (evtMgr) evtMgr->GetTrackingManager()->GetSteppingManager()->SetNavigator(nav);
  fNavigator = nav;
  //   trMgr->DeRegisterNavigator(oldNav);
  Info("SetNavigator", "TG4VecGeomNavigator created and registered to G4TransportationManager");
}

//______________________________________________________________________________
void TG4VecGeomNavMgr::Initialize(TVirtualUserPostDetConstruction *sdinit, int nthreads)
{
  /// Construct G4 geometry based on TGeo geometry.
  Info("Initialize", "Creating G4 hierarchy ...");
  if (fDetConstruction) fDetConstruction->Initialize(sdinit);
  if (nthreads > 1) {
    // gGeoManager->SetMaxThreads(nthreads);
    Error("Initialize", "Don't know how to handle threads");
  }
}

//______________________________________________________________________________
void TG4VecGeomNavMgr::SetVerboseLevel(int level)
{
  /// Set navigator verbosity level.
  fNavigator->SetVerboseLevel(level);
}

//______________________________________________________________________________
void TG4VecGeomNavMgr::PrintG4State() const
{
  /// Print current G4 state.
  // G4NavigationHistory *history = fNavigator->GetHistory();
  // G4cout << *history << G4endl;
}
